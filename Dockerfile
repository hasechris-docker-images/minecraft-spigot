FROM ubuntu:xenial

ARG DEBIAN_FRONTEND=noninteractive
ARG MC_SERVER_PORT=25565
ARG MC_RCON_PORT=25575

WORKDIR /var/lib/minecraft

RUN apt-get update
RUN apt-get install --yes --no-install-recommends apt-transport-https ca-certificates curl gnupg-agent software-properties-common
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
RUN add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"
RUN apt-get update 
RUN apt-get install --yes --no-install-recommends curl docker-ce-cli docker-ce containerd.io apt-transport-https ca-certificates gnupg-agent jq openjdk-8-jre-headless rsync vim
RUN fileUrl=$(curl --silent --location "https://api.github.com/repos/itzg/rcon-cli/releases/latest" | jq --raw-output '.assets[] | select(.name | contains("linux_amd64.tar.gz")) | .browser_download_url') \
 && curl --silent --location "${fileUrl}" | tar xz -C /usr/local/bin
RUN apt-get autoremove --yes --purge
RUN apt-get clean && rm --recursive --force /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY spigot-*.jar /opt/minecraft/
COPY minecraft/ /etc/minecraft/
COPY bin/ /usr/local/bin/

VOLUME /var/lib/minecraft

EXPOSE ${MC_SERVER_PORT} ${MC_RCON_PORT}

CMD ["/etc/minecraft/start.sh"]
