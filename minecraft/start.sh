#!/bin/bash

#################
##  Functions  ##
#################
doShutdown() {
  if [ ${MC_WORLDS_IN_RAM:-false} == true ]; then
    for LEVEL_NAME in $LEVEL_NAME_FILE_CONTENT; do
      if [ ! $(eval echo "\$SYNC_PID_$(echo "$LEVEL_NAME" | tr [:lower:] [:upper:] | tr '-' '_')") == "" ]; then
        echo "sync Process $LEVEL_NAME found. Sending SIGTERM to it."
        kill -15 $(eval echo "\$SYNC_PID_$(echo "$LEVEL_NAME" | tr [:lower:] [:upper:] | tr '-' '_') ")
        wait $(eval echo "\$SYNC_PID_$(echo "$LEVEL_NAME" | tr [:lower:] [:upper:] | tr '-' '_')")
        if [ ! $? ]; then
          syncerror="true"
        fi
      fi
    done
    #eval "SYNC_PID_${LEVEL_NAME}"="$!"
    #printenv | grep "SYNC_PID"
  else
    if [ ! -z ${SYNC_PID} -a -d /proc/${SYNC_PID} ]; then
        echo -en 'Stopping continuous sync...'
        kill ${SYNC_PID}
        wait ${SYNC_PID}
        echo -e '\e[42mdone\e[49m'
    fi
  fi
  
  if [ ! -z ${JAVA_PID} -a -d /proc/${JAVA_PID} ]; then
    echo -en 'Stopping SpigotMC...'
    echo -e "say This server will be shutdown in 15 seconds" > $JAVA_FIFO
    echo -e "save-all" > $JAVA_FIFO
    sleep 15
    echo -e "stop" > $JAVA_FIFO
  fi

  # loop to detect if java process is running
  i=0
  while [ ! -z ${JAVA_PID} -a -d /proc/${JAVA_PID} ]
  do
    echo "Testing if JAVA is stopped propperly... $i"
    sleep 5
    if [ $i -gt 5 ]; then
      echo -e "ERROR on MC Server Shutdown. Please examine log"
      kill -9 ${JAVA_PID}
      syncerror="true"
      break
    fi
    i=$(( $i+1 ))
  done

  echo -e '\e[42mdone\e[49m'
  sleep 5

  echo -en "Kill tail..."
  kill -15 ${TAIL_PID}
  wait ${TAIL_PID}
  echo -e '\e[42mdone\e[49m'

  echo -en 'Performing final sync...'
  if [ ${MC_WORLDS_IN_RAM:-false} == true ]; then
    for LEVEL_NAME in $LEVEL_NAME_FILE_CONTENT; do
      if [ ! $(eval echo "\$SYNC_PID_$(echo "$LEVEL_NAME" | tr [:lower:] [:upper:] | tr '-' '_')") == "" ]; then
        for DIMENSION in ${LEVEL_NAME}{,_nether,_the_end}; do
          rsync -vaph --delete /dev/shm/$(hostname)/${DIMENSION} /var/lib/minecraft/worldstore || syncerror="true"
          if [ "/dev/shm/$(hostname)/${DIMENSION}" -nt "/var/lib/minecraft/worldstore/${DIMENSION}" ]; then
            syncerror="true"
          fi
        done
      fi
    done
  else
    if [ ! -z ${SYNC_PID} -a ! -d /proc/${SYNC_PID} ]; then
      rsync -vaph --delete /dev/shm/$(hostname)/${DIMENSION} /var/lib/minecraft/worldstore
      if [ -z $syncerror && "/dev/shm/$(hostname)/${DIMENSION}" -nt "/var/lib/minecraft/worldstore/${DIMENSION}" ]; then
        syncerror="true"
      fi
    else
      syncerror="true"
    fi
  fi

  if [[ "$syncerror" != "true" ]]; then
    echo -e '\e[42mdone\e[49m'
    echo -e '\e[42mCleaning out the SHM Folder\e[49m'
    rm -r /dev/shm/$(hostname)
    echo -e '\e[45mCLEAN SHUTDOWN :D\e[49m'
  else
    echo -e 'Sync ERROR!! Not deleting world folders in SHM Folder'
    exit 1
  fi

  exit 0
}

doConfigUpdate() {
    for FILE in eula.txt server.properties; do
        echo -e "Updating \e[94m${FILE}\e[39m:"
        FILE_CHANGED=false

        for KEY in $(grep ^[[:lower:]] ${FILE} | cut -d= -f1); do
            VAR="MC_$(tr [:punct:] _ <<< ${KEY} | tr [:lower:] [:upper:])"

            if [ -v ${VAR} ]; then
                echo -e "\t\e[95m${KEY}\e[39m=\e[96m${!VAR}\e[39m"
                sed --in-place --regexp-extended \
                --expression "s/^(${KEY}=).*/\1${!VAR}/" \
                ${FILE}
                FILE_CHANGED=true
            fi
        done

        if [ ${FILE_CHANGED} == false ]; then
            echo -e '\t\e[93mNothing changed!\e[39m'
        fi
    done

    if [ ${MC_LINK_DRIVEBACKUP_CONFIG:-false} == true ]; then
      if [ ! -L /var/lib/minecraft/plugins/DriveBackupV2/config.yml ]; then
        mv /var/lib/minecraft/plugins/DriveBackupV2/config.yml /var/lib/minecraft/plugins/DriveBackupV2/config.yml.old
        echo -n "Updating config file for DriveBackupV2..."
        mkdir -p "plugins/DriveBackupV2"
        ln -s /var/lib/minecraft/drivebackup_config.yml /var/lib/minecraft/plugins/DriveBackupV2/config.yml
        echo -e '\e[42mdone\e[49m'
      fi
    fi
}

doWorldMaintenance() {
    # grep level name from stdin
    LEVEL_NAME="$1"

    # loop over the dimensions
    for DIMENSION in ${LEVEL_NAME}{,_nether,_the_end}; do
        echo -e "Performing maintenance on \e[92m${DIMENSION}\e[39m:"
        DIMENSION_CHANGED=false

        if [ -d ${DIMENSION} -a ! -L ${DIMENSION} ]; then
            echo -en "\tMoving to \e[94mworldstore\e[39m..."
            rsync --archive --remove-source-files ${DIMENSION} worldstore
            find ${DIMENSION} -type d -empty -delete
            echo -e '\e[42mdone\e[49m'
            DIMENSION_CHANGED=true
        elif [ ! -d worldstore/${DIMENSION} ]; then
            if [ ${FIRST_RUN:-false} == true ]; then
                echo -e "\t\e[93mSkipping. Please restart the container after worlds are generated.\e[39m"
            else
                echo -e "\t\e[93mCannot find directory! Skipping.\e[39m"
            fi
            continue
        fi
    done
}

moveWorldToRAM() {
    #grep WORLD Name from stdin
    LEVEL_NAME="$1"

    for DIMENSION in ${LEVEL_NAME}{,_nether,_the_end}; do
      if [ ${MC_WORLDS_IN_RAM:-false} == true ]; then
          echo -en "\tSyncing to \e[94m/dev/shm/$(hostname)/${DIMENSION}\e[39m..."
          SHM_SIZE=$(df --output=size /dev/shm | sed 1d)
          SHM_MIN=$((1024 * 1024))
          SHM_AVAIL=$(df --output=avail /dev/shm | sed 1d)
          DIMENSION_SIZE=$(du --summarize worldstore/${DIMENSION} | cut -f1)
          REQD_AVAIL=$((${DIMENSION_SIZE} * 120 / 100))

          if [ ${SHM_SIZE} -ge ${SHM_MIN} -a ${SHM_AVAIL} -ge ${REQD_AVAIL} ]; then
              rsync --archive worldstore/${DIMENSION} /dev/shm/$(hostname)
              ln --symbolic --force /dev/shm/$(hostname)/${DIMENSION}
              echo -e '\e[42mdone\e[49m'

              DIMENSION_CHANGED=true
          else
              echo -e '\e[41mfailed\e[49m'
              SHM_DIFF=$((${SHM_MIN} - ${SHM_SIZE}))
              REQD_DIFF=$((${REQD_AVAIL} - ${SHM_AVAIL}))

              if [ ${SHM_DIFF} -ge ${REQD_DIFF} ]; then
                  echo -e "\t\t\e[94m/dev/shm\e[93m must be a minumum of ${SHM_MIN}K (\e[91m${SHM_DIFF}K needed\e[93m).\e[39m"
              else
                  echo -e "\t\t\e[94m/dev/shm\e[93m does not have enough free space (\e[91m${REQD_DIFF}K needed\e[93m)).\e[39m"
              fi

              ln --symbolic --force worldstore/${DIMENSION}
          fi
      else
          ln --symbolic --force worldstore/${DIMENSION}
      fi
  done
}

#doSync() {
#    LEVEL_NAME="$1"
#    for WORLD in ${LEVEL_NAME}{,_nether,_the_end}; do
#        if [ -d /dev/shm/$(hostname)/${WORLD} ]; then
#            rsync -vad /dev/shm/$(hostname)/${WORLD} worldstore
#        fi
#    done
#}

#################
##  MAIN       ##
#################
# variablen
LEVEL_NAME_FILE="/var/lib/minecraft/level_names_ram"

# loop to detect and ignore world names with comment sign '#' in the beginning
if [ ${MC_READ_LEVEL_FILE:-false} == true -a -f $LEVEL_NAME_FILE ]; then
  for content in $(cat $LEVEL_NAME_FILE) 
  do
    if [[ $content =~ ^#.* ]]; then
        echo -e "World: $content\tbegins with #. Skipping..."
        continue
      else
        LEVEL_NAME_FILE_CONTENT+="$content\n"
     fi
  done
  
  # remove all empty lines
  LEVEL_NAME_FILE_CONTENT=$(echo -e "${LEVEL_NAME_FILE_CONTENT}" | sed '/^[[:space:]]*$/d')
else
  LEVEL_NAME=$(grep ^level-name server.properties | cut -d= -f2)
fi

JAVA_FIFO="/tmp/JAVA_FIFO_MINECRAFT"

if [ ${MC_EULA:-false} != true ]; then
    echo -e '\e[41m!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\e[49m'
    echo -e '\e[41mMC_EULA must always be set to true.\e[49m'
    echo -e '\e[41mPlease indicate you agree to the EULA (https://account.mojang.com/documents/minecraft_eula).\e[49m'
    echo -e '\e[41m!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\e[49m'
    exit
fi

if [ ${MC_READ_LEVEL_FILE:-false} == true -a ! -f $LEVEL_NAME_FILE ]; then
    echo -e '\n\e[44m##############################\e[49m'
    echo -e '\e[44mERROR! You specified to load level names from RAM file.\e[49m'
    echo -e "\e[44mERROR! But you forgot to create the file $LEVEL_NAME_FILE. Exiting...\e[49m"
    echo -e '\e[44m##############################\e[49m'
    exit
fi

if [ ! -f eula.txt -o ! -f server.properties ]; then
    echo -e '\e[44m##############################\e[49m'
    echo -e '\e[44mPerforming first-time setup.\e[49m'
    echo -e '\e[44mErrors and warnings regarding server.properties and/or eula.txt are expected.\e[49m'
    echo -e '\e[44m##############################\e[49m'
    FIRST_RUN=true

    $(which java) -jar /opt/minecraft/spigot-*.jar
fi

# create fifo file is not existing
if [ ! -p $JAVA_FIFO ]; then
  mkfifo $JAVA_FIFO
fi


# when SIGTERM is detected run the shutdown function which cleans all up and exits the script
trap 'doShutdown' SIGTERM

# trap on strg-C
#trap 'doShutdown' 2

# first we do the Config Update
doConfigUpdate

##################
##  MAINTENANCE ##
## on Worlds &  ##
##  Dimensions  ##
##################
#

#set -x
## big IF which decides if level names are read from level name file or from server.properties file
if [ ${MC_READ_LEVEL_FILE:-false} == true ]; then
  #grep level names from file
  if [ -d /dev/shm/$(hostname) ]; then
    echo "ERROR: Found Folder with hostname in shared memory. Assuming the last shutdown was unsuccessfull. NOT moving world folders to shared memory, instead starting with supplied ones"
    for LEVEL_NAME in $LEVEL_NAME_FILE_CONTENT; do
      doWorldMaintenance "$LEVEL_NAME"
    done
  else
    for LEVEL_NAME in $LEVEL_NAME_FILE_CONTENT; do
      doWorldMaintenance "$LEVEL_NAME"
      moveWorldToRAM "$LEVEL_NAME"
    done
  fi
else
    #LEVEL_NAME=$(grep ^level-name server.properties | cut -d= -f2)
    doWorldMaintenance "$LEVEL_NAME"
    if [ -d /dev/shm/$(hostname) ]; then
      echo "ERROR: Found Folder with hostname in shared memory. Assuming the last shutdown was unsuccessfull. NOT moving world folders to shared memory, instead starting with supplied ones"
    else
      moveWorldToRAM "$LEVEL_NAME"
    fi
fi

tail -f $JAVA_FIFO | $(which java) \
    -Dserver.name=${MC_SERVER_NAME:-minecraft} \
    -Xms${MC_MIN_MEM:-1G} \
    -Xmx${MC_MAX_MEM:-2G} \
    ${MC_JAVA_ARGS} \
    -jar /opt/minecraft/spigot-*.jar \
    nogui &

JAVA_PID=$!
TAIL_PID=$(pgrep -f 'tail')

#if [ ${FIRST_RUN:-false} == false ]; then
#    sleep 1m
#else
#    sleep 3m
#fi

# RUN sync job in Background
#$(which sync.sh) -c &
# get process ID from sync job
#SYNC_PID=$!
if [ ${MC_WORLDS_IN_RAM:-false} == true ] && [ ${FIRST_RUN:-false} == false ]; then
  echo "Going to sync worlds to worldstore folder..."
  # grep level names from file
  for LEVEL_NAME in $LEVEL_NAME_FILE_CONTENT; do
    #sleep infinity &
    $(which sync.sh) -c --level-name "$LEVEL_NAME" &
    #eval "SYNC_PID_$(echo $LEVEL_NAME | tr [:lower:] [:upper:] | tr '-' '_' )"="$!"
    #eval echo "SYNC_PID_${LEVEL_NAME}"
    #printenv | grep "SYNC_PID"
  done
else
  $(which sync.sh) -c --level-name "$LEVEL_NAME" &
  SYNC_PID=$!
fi

sleep 10

# do nothing else for eternity (minecraft and sync job are running in this shell in background)
while true; do
#wait
  sleep 10
  if [ ! -z ${JAVA_PID} -a ! -d /proc/${JAVA_PID} ]; then
    echo "Issuing docker stop command"
    docker container stop -t 90 $(hostname)
  elif [ ! -z ${TAIL_PID} -a ! -d /proc/${TAIL_PID} ]; then
    echo "Issuing docker stop command"
    docker container stop -t 90 $(hostname)
  fi
done
